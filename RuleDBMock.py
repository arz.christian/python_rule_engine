def load_rule(rule_id):
    rules = dict()
    rules[1] = "( {1}==2 || E==F ) && D==D && {3}==3 -- {T} ELSE {F}"
    rules[2] = "( A==B || ( 2==2 || E==F ) ) && D==D && 10==10 -- {T} ELSE {F}"
    return rules[rule_id]

def load_variable(variable_id):
    variables = dict()

    variables["1"] = 1
    variables["2"] = 2
    variables["3"] = 3
    variables["4"] = 4
    variables["5"] = 5
    variables["6"] = 6
    variables["7"] = 7
    variables["8"] = 8
    variables["9"] = 9

    return variables[variable_id]