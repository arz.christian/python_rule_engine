final_result = None
def evaluate_tree(tree):
    print "in evaluate_tree"
    print tree
    for key, node in tree.iteritems():
        if node.is_leave:
            print ("Node: " + node.data + ", is_leave: " + str(node.is_leave))
            value = False
            if "==" in node.data:
                splitted = node.data.split("==")
                value = equal(splitted)
            elif ">" in node.data:
                splitted = node.data.split(">")
                value = greater(splitted)
            elif ">=" in node.data:
                splitted = node.data.split(">=")
                value = greater_equal(splitted)
            elif "<" in node.data:
                splitted = node.data.split("<")
                value = smaller(splitted)
            elif ">=" in node.data:
                splitted = node.data.split(">=")
                value = smaller_equal(splitted)
            elif "!=" in node.data:
                splitted = node.data.split("!=")
                value = not_equal(splitted)
            node.data = value

    evaluate_expressions(tree)
    print ("Final Result: " + str(final_result))
    global final_result
    return final_result


def evaluate_expressions(tree):
    evaluation_list = list()
    broke = True
    for key, node in tree.iteritems():
        if not node.is_leave and broke:
            for key2, node2 in tree.iteritems():
                broke = False
                if node2.parent_node == node.node_id and node2.is_leave:
                    evaluation_list.append(node2)
                elif node2.parent_node == node.node_id and not node2.is_leave:
                    evaluation_list = list()
                    broke = True
                    break
        if not broke:
            break

    if len(evaluation_list) > 1:
        result = None
        for key, node in tree.iteritems():
            print ("nodelist: " + str(key) + ": " + node.to_string())
        for node in evaluation_list:
            print ("evaluationlist: " + node.to_string())
        parent_node = tree[evaluation_list[0].parent_node]
        if parent_node.data == "&&":
            result = and_operation(evaluation_list)
        else:
            result = or_operation(evaluation_list)

        tree[parent_node.node_id].data = result
        tree[parent_node.node_id].is_leave = True

        for node in evaluation_list:
            tree.pop(node.node_id)

        evaluate_expressions(tree)
    else:
        global final_result
        for key, node in tree.iteritems():
            final_result = node.data
            print ("Set Final Result to: " + str(node.data))
        for key, node in tree.iteritems():
            print ("nodelist: " + str(key) + ": " + node.to_string())
        for node in evaluation_list:
            print ("evaluationlist: " + node.to_string())
        print final_result



def equal (parameters):
    if parameters[0] == parameters[1]:
        return True
    else:
        return False


def greater (parameters):
    if parameters[0] > parameters[1]:
        return True
    else:
        return False


def greater_equal (parameters):
    if parameters[0] >= parameters[1]:
        return True
    else:
        return False


def smaller (parameters):
    if parameters[0] < parameters[1]:
        return True
    else:
        return False


def smaller_equal (parameters):
    if parameters[0] <= parameters[1]:
        return True
    else:
        return False


def not_equal (parameters):
    if not parameters[0] == parameters[1]:
        return True
    else:
        return False

def and_operation(parameters):
    result = True
    for parameter in parameters:
        if not parameter.data:
            result = False
            break
    return result


def or_operation (parameters):
    result = False
    for parameter in parameters:
        if parameter.data:
            result = True
            break
    return result