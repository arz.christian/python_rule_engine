class TreeObject (object):

    def __init__(self, parent, data, id, is_leave):
        self.parent_node = parent
        self.data = data
        self.node_id = id
        self.is_leave = is_leave

    def to_string (self):
        string = "data: " + str(self.data) + ", node_id: " + str(self.node_id) + ", is_leave: " + str(self.is_leave)
        if self.parent_node is not None:
            string = string + ", parent: " + str(self.parent_node)

        return string