from TreeObject import TreeObject
from TreeEvaluator import evaluate_tree
from RuleDBMock import load_rule
from RuleDBMock import load_variable

nodes_list = dict ()
root_node = None
current_node_id = 0
result = None


def build_rule_tree(input):
    global current_node_id
    global nodes_list

    print input

    i = 0
    end_of_innermost_phrase = None
    start_of_innermost_phrase = None
    for end_part in input:
        if ")" in end_part:
            end_of_innermost_phrase = i
            break
        i = i + 1

    i = 0
    if end_of_innermost_phrase:
        for start_part in input:
            if i < end_of_innermost_phrase and "(" in start_part:
                start_of_innermost_phrase = i
            if i >= end_of_innermost_phrase:
                break
            i = i + 1
    else:
        start_of_innermost_phrase = 0
        end_of_innermost_phrase = len(input) - 1

    print input
    print ("start: " + str(start_of_innermost_phrase) + " end: " + str(end_of_innermost_phrase))

    parent_node = None

    for i in range(start_of_innermost_phrase+1, end_of_innermost_phrase):
        if "$" not in input[i]:
            parent_node = TreeObject(None, input[i], current_node_id, False)
            nodes_list [current_node_id] = parent_node
            current_node_id = current_node_id + 1
            break

    for i in range (start_of_innermost_phrase, end_of_innermost_phrase+1):
        print ("analyzing node: " + input[i])
        if "$" in input[i]:
            node_id = int(input[i].split("$")[1])
            print ("Trying to acces node id " + str(node_id) + " to set parent node id: " + str(parent_node.node_id))
            nodes_list[node_id].parent_node = parent_node.node_id

    for i in range (start_of_innermost_phrase, end_of_innermost_phrase):
        input.pop(start_of_innermost_phrase + 1)

    if len(input) > 1:
        input[start_of_innermost_phrase] = "$" + str(parent_node.node_id)
        build_rule_tree(input)
    else:
        for key, node in nodes_list.iteritems():
            print (str(key) + ": " + node.to_string())


def parse_rule(rule_id):
    rule = load_rule(rule_id)
    splitted = rule.split("--")
    global result
    result = splitted[1]
    evaluation = splitted[0]
    evaluation = inject_variables(evaluation)
    leaves_string = parse_leaves(evaluation)
    build_rule_tree (leaves_string)


def parse_leaves(rule):
    global parent_node
    global current_node_id
    global nodes_list

    rule.strip()

    splitted = rule.split(" ")

    i = 0
    for part in splitted:
        if not part:
            splitted.pop(i)
        if "=" in part or "<" in part or ">" in part:
            nodes_list[current_node_id] = TreeObject(None, part, current_node_id, True)
            splitted[i] = "$" + str(current_node_id)
            current_node_id = current_node_id + 1
        i = i + 1

    print splitted
    return splitted


def inject_variables(evaluation):
    evaluation_string = evaluation
    while "{" in evaluation_string:
        variable_string = evaluation_string.split("{", 1)
        variable_string = variable_string[1].split("}", 1)
        variable_id = variable_string[0]
        variable = load_variable(variable_id[0])
        print ("Variable: " + str(variable))
        evaluation_string = variable_string[1]
        string_to_replace = "{" + variable_id + "}"
        print evaluation_string
        evaluation = evaluation.replace(string_to_replace, str(variable))
        print ("String to replace: " + string_to_replace + ", evaluation: " + evaluation)

    print ("evaluation: " + evaluation)
    return evaluation

parse_rule(1)
evaluate_tree(nodes_list)